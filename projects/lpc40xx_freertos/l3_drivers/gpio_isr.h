// @file gpio_isr.h
// Including extra credit portion
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

// include this file at gpio_lab.c file
#include "lpc40xx.h"

typedef enum {
  GPIO_INTR__FALLING_EDGE,
  GPIO_INTR__RISING_EDGE,
} gpio_interrupt_e;

// Function pointer type (demonstrated later in the code sample)
typedef void (*function_pointer_t)(void);

// Allow the user to attach their callbacks
void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback);

// Allow the user to attach their callbacks
void gpio2__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback);

// Our main() should configure interrupts to invoke this dispatcher where we will invoke user attached callbacks
// You can hijack 'interrupt_vector_table.c' or use API at lpc_peripherals.h
void gpio__interrupt_dispatcher(void);

// helper function to get the pin number that generated the interrupt
// pin parameter becomes -1 if no pin triggered the interrupt
void get_int_generated_pin(uint8_t port, int *pin, gpio_interrupt_e *triggered_edge);