// @file gpio_isr.c
#include "gpio_isr.h"

// Note: You may want another separate array for falling vs. rising edge callbacks
static function_pointer_t gpio0_callbacks_rising[32];
static function_pointer_t gpio0_callbacks_falling[32];
static function_pointer_t gpio2_callbacks_rising[32];
static function_pointer_t gpio2_callbacks_falling[32];

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  // 1) Store the callback based on the pin at gpio0_callbacks
  if (pin >= 0 && pin <= 31) {
    if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
      gpio0_callbacks_falling[pin] = callback;

      // 2) Configure GPIO 0 pin for falling edge
      LPC_GPIOINT->IO0IntEnF |= (1 << pin);
    } else if (interrupt_type == GPIO_INTR__RISING_EDGE) {
      gpio0_callbacks_rising[pin] = callback;

      // 2) Configure GPIO 0 pin for rising edge
      LPC_GPIOINT->IO0IntEnR |= (1 << pin);
    }
  }
}

void gpio2__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  // 1) Store the callback based on the pin at gpio2_callbacks
  if (pin >= 0 && pin <= 31) {
    if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
      gpio2_callbacks_falling[pin] = callback;

      // 2) Configure GPIO 2 pin for falling edge
      LPC_GPIOINT->IO2IntEnF |= (1 << pin);
    } else if (interrupt_type == GPIO_INTR__RISING_EDGE) {
      gpio2_callbacks_rising[pin] = callback;

      // 2) Configure GPIO 2 pin for rising edge
      LPC_GPIOINT->IO2IntEnR |= (1 << pin);
    }
  }
}

// We wrote some of the implementation for you
void gpio__interrupt_dispatcher(void) {
  // check which port generated the interrupt
  if (LPC_GPIOINT->IntStatus & 1) // port 0
  {
    // Check which pin and which edge generated the interrupt
    int pin_that_generated_interrupt = -1;
    gpio_interrupt_e edge_that_generated_interrupt;
    get_int_generated_pin(0, &pin_that_generated_interrupt, &edge_that_generated_interrupt);

    function_pointer_t attached_user_handler = NULL;

    if (pin_that_generated_interrupt != -1 && edge_that_generated_interrupt == GPIO_INTR__FALLING_EDGE) {
      attached_user_handler = gpio0_callbacks_falling[pin_that_generated_interrupt];
    } else if (pin_that_generated_interrupt != -1 && edge_that_generated_interrupt == GPIO_INTR__RISING_EDGE) {
      attached_user_handler = gpio0_callbacks_rising[pin_that_generated_interrupt];
    }

    // Invoke the user registered callback, and then clear the interrupt
    attached_user_handler();
    LPC_GPIOINT->IO0IntClr |= 1 << pin_that_generated_interrupt;
  }

  else if (LPC_GPIOINT->IntStatus & (1 << 2)) // port 2
  {
    // Check which pin and which edge generated the interrupt
    int pin_that_generated_interrupt = -1;
    gpio_interrupt_e edge_that_generated_interrupt;
    get_int_generated_pin(2, &pin_that_generated_interrupt, &edge_that_generated_interrupt);

    function_pointer_t attached_user_handler = NULL;

    if (pin_that_generated_interrupt != -1 && edge_that_generated_interrupt == GPIO_INTR__FALLING_EDGE) {
      attached_user_handler = gpio2_callbacks_falling[pin_that_generated_interrupt];
    } else if (pin_that_generated_interrupt != -1 && edge_that_generated_interrupt == GPIO_INTR__RISING_EDGE) {
      attached_user_handler = gpio2_callbacks_rising[pin_that_generated_interrupt];
    }

    // Invoke the user registered callback, and then clear the interrupt
    attached_user_handler();
    LPC_GPIOINT->IO2IntClr |= 1 << pin_that_generated_interrupt;
  }
}

void get_int_generated_pin(uint8_t port, int *pin, gpio_interrupt_e *triggered_edge) {
  uint32_t port_value_rising_edge = 0;
  uint32_t port_value_falling_edge = 0;
  if (port == 0) {
    port_value_rising_edge = LPC_GPIOINT->IO0IntStatR;
    port_value_falling_edge = LPC_GPIOINT->IO0IntStatF;
  } else if (port == 2) {
    port_value_rising_edge = LPC_GPIOINT->IO2IntStatR;
    port_value_falling_edge = LPC_GPIOINT->IO2IntStatF;
  } else {
    fprintf(stderr, "port needs to be 0 or 2");
  }
  // Loop through the port value, shifting until value becomes 0
  int triggered_pin = -1;
  while (port_value_rising_edge != 0 && triggered_pin < 32) {
    port_value_rising_edge >>= 1;
    triggered_pin++;
  }
  if (triggered_pin == -1) // rising edge not obtained, moving unto falling edge
  {
    while (port_value_falling_edge != 0 && triggered_pin < 32) {
      port_value_falling_edge >>= 1;
      triggered_pin++;
    }
    *triggered_edge = GPIO_INTR__FALLING_EDGE;
  } else // triggered_pin is found as rising
  {
    *triggered_edge = GPIO_INTR__RISING_EDGE;
  }
  *pin = triggered_pin;
}