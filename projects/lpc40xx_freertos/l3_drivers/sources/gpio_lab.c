#include "gpio_lab.h"
#include <stdio.h>

/// Should alter the hardware registers to set the pin as input
void gpio0__set_as_input(uint8_t pin_num) {
  if (pin_num >= 0 && pin_num <= 31)
    LPC_GPIO0->DIR &= ~(1 << pin_num);
}

/// Should alter the hardware registers to set the pin as output
void gpio0__set_as_output(uint8_t pin_num) {
  if (pin_num >= 0 && pin_num <= 31)
    LPC_GPIO0->DIR |= (1 << pin_num);
}

/// Should alter the hardware registers to set the pin as high
void gpio0__set_high(uint8_t pin_num) {
  if (pin_num >= 0 && pin_num <= 31)
    LPC_GPIO0->SET = (1 << pin_num);
}

/// Should alter the hardware registers to set the pin as low
void gpio0__set_low(uint8_t pin_num) {
  if (pin_num >= 0 && pin_num <= 31)
    LPC_GPIO0->CLR = (1 << pin_num);
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio0__set(uint8_t pin_num, bool high) {
  if (pin_num >= 0 && pin_num <= 31) {
    if (high == true) {
      LPC_GPIO0->PIN |= (1 << pin_num);
    } else {
      LPC_GPIO0->PIN &= (1 << pin_num);
    }
  }
}

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool gpio0__get_level(uint8_t pin_num) {
  bool result = false;
  if (pin_num >= 0 && pin_num <= 31) {
    if (LPC_GPIO0->PIN & (1 << pin_num)) {
      result = true;
    }
  }
  return result;
}

//////////////Functions used for all ports///////////////////////
void lab_gpio__set_as_input(uint8_t port_num, uint8_t pin_num) {
  // port number must be between 0 and 4, and pin number must be between 0 and 31
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    port_ptr->DIR &= ~(1 << pin_num);
  }
}

void lab_gpio__set_as_output(uint8_t port_num, uint8_t pin_num) {
  // port number must be between 0 and 4, and pin number must be between 0 and 31
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    port_ptr->DIR |= 1 << pin_num;
  }
}

void lab_gpio__set_high(uint8_t port_num, uint8_t pin_num) {
  // port number must be between 0 and 4, and pin number must be between 0 and 31
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    port_ptr->SET = 1 << pin_num;
  }
}

void lab_gpio__set_low(uint8_t port_num, uint8_t pin_num) {
  // port number must be between 0 and 4, and pin number must be between 0 and 31
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    port_ptr->CLR = 1 << pin_num;
  }
}

void lab_gpio__set(uint8_t port_num, uint8_t pin_num, bool high) {
  // port number must be between 0 and 4, and pin number must be between 0 and 31
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    if (high == true) {
      port_ptr->PIN |= 1 << pin_num;
    } else {
      port_ptr->PIN &= ~(1 << pin_num);
    }
  }
}

bool lab_gpio__get_level(uint8_t port_num, uint8_t pin_num) {
  bool result = false;
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    if (port_ptr->PIN & (1 << pin_num)) {
      result = true;
    }
  }
  return result;
}

void lab_gpio__toggle(uint8_t port_num, uint8_t pin_num) {
  if (port_num >= 0 && port_num <= 4 && pin_num >= 0 && pin_num <= 31) {
    LPC_GPIO_TypeDef *port_ptr = NULL;
    switch (port_num) {
    case 0:
      port_ptr = LPC_GPIO0;
      break;
    case 1:
      port_ptr = LPC_GPIO1;
      break;
    case 2:
      port_ptr = LPC_GPIO2;
      break;
    case 3:
      port_ptr = LPC_GPIO3;
      break;
    default: // case 4
      port_ptr = LPC_GPIO4;
    }
    port_ptr->PIN ^= (1 << pin_num);
  }
}

/////////////////interrupt functions//////////////////////////////

void lab_interrupt_enable(uint8_t port_num, uint8_t pin_num, Edge edge) {
  if ((port_num == 0 || port_num == 2) && pin_num >= 0 && pin_num <= 31) {
    switch (port_num) {
    case 0:
      if (edge == RISING) {
        LPC_GPIOINT->IO0IntEnR |= (1 << pin_num);
      } else {
        LPC_GPIOINT->IO0IntEnF |= (1 << pin_num);
      }
      break;
    default: // case 2
      if (edge == RISING) {
        LPC_GPIOINT->IO2IntEnR |= (1 << pin_num);
      } else {
        LPC_GPIOINT->IO2IntEnF |= (1 << pin_num);
      }
    }
  }
}

void lab_interrupt_disable(uint8_t port_num, uint8_t pin_num, Edge edge) {
  if ((port_num == 0 || port_num == 2) && pin_num >= 0 && pin_num <= 31) {
    switch (port_num) {
    case 0:
      if (edge == RISING) {
        LPC_GPIOINT->IO0IntEnR &= ~(1 << pin_num);
      } else {
        LPC_GPIOINT->IO0IntEnF &= ~(1 << pin_num);
      }
      break;
    default: // case 2
      if (edge == RISING) {
        LPC_GPIOINT->IO2IntEnR &= ~(1 << pin_num);
      } else {
        LPC_GPIOINT->IO2IntEnF &= ~(1 << pin_num);
      }
    }
  }
}

void lab_interrupt_clear(uint8_t port_num, uint8_t pin_num) {
  if ((port_num == 0 || port_num == 2) && pin_num >= 0 && pin_num <= 31) {
    switch (port_num) {
    case 0:
      LPC_GPIOINT->IO0IntClr |= (1 << pin_num);
      break;
    default: // case 2
      LPC_GPIOINT->IO2IntClr |= (1 << pin_num);
    }
  }
}